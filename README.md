�E�EN�
========

�e�en� �kolu, kter� budu d�le popisovat jsem vygeneroval 
pomoc� p�ilo�en�ho skriptu "align.py" p��kazem:
$ python align.py > result.log

Zad�n�m �kolu bylo spo��tat glob�ln� a lok�ln� alignment pro DNA sequence.
DNA sequence ulo�en� v souberech H.fasta, I.fasta a J.fasta byly poskytnut�.
Zbyl� sequence jsem st�hl do soubor� sequence h.fasta, r.fasta, m.fasta.
V�echny soubory s DNA obsahuj� pouze 1 ��dku s danou DNA sequenc�. 
S takto naform�tovan�mi soubory pracuje "align.py" skript.


TABULKA zdroj� pro sta�en� soubory:
h.fasta: Homo sapiens insulin (INS), transcript variant 1, mRNA
    Downloaded from http://www.ncbi.nlm.nih.gov/nuccore/109148525?report=fasta
r.fasta: Rattus norvegicus insulin 1 (Ins1), mRNA
    Downloaded from http://www.ncbi.nlm.nih.gov/nuccore/297374813?report=fasta
m.fasta: Mus musculus insulin II (Ins2), transcript variant 1, mRNA
    Downloaded from http://www.ncbi.nlm.nih.gov/nuccore/297374793?report=fasta


Glob�ln� alignment byl spo��t�n pro dv� nastaven�. 
S afinn�my parametry (gapopen = -2, gapext = -1) ve funkci "task2"
a s parametry (gapopen = gapext = -2) ve funkci "task1ab".
Naimplementoval jsem jak afinn� variantu, tak neafinn� verzi, 
p�esto�e poprv� jsem vy�e�il neaffin� variantu pomoc� affin� s gapopen = gapext. 
V�hoda zvl�tn�ho �e�en� je p�edev��m pam�ov� �spora zhruba o polovinu
oproti affin�mu algoritmu s dv�ma tabulkama.

V�SLEDKY
========
Tabulka n��e ukazuje glob�ln� a za lom�tkem lok�ln� alignment pro dvojce sequenc�.
Z tabulky je jasn� vid�t, �e sekvence mohou b�t glob�ln� pom�rn� stejn� NEpodobn�,
ale mohou obsahovat velmi dlouh� podobn� podsekvence (sekvence {I,H} vs {J,H}).

====== Needleman-Wunsch and Smith-Watermann ======
 glob/loc   H.fasta I.fasta J.fasta
H.fasta     skip 
I.fasta    -214/1292  skip 
J.fasta    -206/727 1020/1042  skip 

====== Afinne Needleman-Wunsch ======
 glob   h.fasta m.fasta r.fasta
h.fasta     skip 
m.fasta    229  skip 
r.fasta    215 352  skip 

Pro alignmenty se pod�vejte do souboru "results.log". 
V sekci "====== Details ... =====" najdete pro ka�d� p�r sekvenc� jejich sk�re,
za��tek, konec v jednotliv�ch aligment� v sequenc�ch a tak� alignment. 
Alignment se nach�z� na dvou po sob� jdouc�ch ��dk�ch, kter� jsou velmi dlouh�.
Je proto vhodn� nastavit textov� editor, aby dlouh� ��dky nezalamoval. (Ve Vimu p��kazem :set nowrap).
Mezera v alignmentu je nazna�en� poml�kou.
Pozn�mka: Indexy do sequenc�, kde se nach�z� alignment jsou indexov�ny od 0! 
