#!/usr/bin/env python
# Author:   Ondrej Platek, Copyright 2012, code is without any warranty!
# Created:  14:33:31 20/12/2012
# Modified: 14:33:31 20/12/2012
import numpy as np


def NeedlemanWunsh(seqA, seqB, match, mismatch, gap):
    # assert mismatch <= 2 * gapext, 'Algorithm works for mismatch < 2 * gapext, where gapext & mismatch < 0!'
    n, m = len(seqA), len(seqB)

    # Dynamic programming initialization
    F = np.empty((n + 1, m + 1))

    # Filling first column and row
    for i in xrange(n):
        F[i, 0] = i * gap
    for j in xrange(m):
        F[0, j] = j * gap
    # Dynamic programming forward
    for i in xrange(n):
        for j in xrange(m):
            if seqA[i] == seqB[j]:
                mx = F[i, j] + match
            else:
                mx = F[i, j] + mismatch
            mx = max(F[i, j + 1] + gap, mx)
            mx = max(F[i + 1, j] + gap, mx)
            F[i + 1, j + 1] = mx
    score = F[n, m]
    # traceback
    alignA, alignB = [], []
    im, jm = n, m
    while im > 0 and jm > 0:
        diag, gapA, gabB = F[im - 1, jm - 1], F[im, jm - 1], F[im - 1, jm]
        maxi = max(diag, gapA, gabB)
        if  maxi == diag:
            im, jm = im - 1, jm - 1
            alignA.append(seqA[im])
            alignB.append(seqB[jm])
        elif maxi == gapA:
            jm = jm - 1
            alignA.append('-')
            alignB.append(seqB[jm])
        else:   # gabB
            im = im - 1
            alignA.append(seqA[im])
            alignB.append('-')
    alignA.reverse()
    alignB.reverse()
    alignA = ''.join(alignA)
    alignB = ''.join(alignB)
    # we are computing global alignment so start is at 0 for both sequences and
    # we cover both sequences fully
    return (score, 0, 0, n - 1, m - 1, alignA, alignB)


def NWAffine(seqA, seqB, match, mismatch, gapopen, gapext):
    # assert mismatch <= 2 * gapext, 'Algorithm works for mismatch < 2 * gapext, where gapext & mismatch < 0!'
    n, m = len(seqA), len(seqB)

    # Dynamic programming initialization
    I = np.empty((n + 1, m + 1))
    M = np.empty((n + 1, m + 1))
    # first row infinity
    mininfinity = min(mismatch, gapopen, gapext) * max(n, m) + 1
    # Setting only [0,0] for M and I
    M[0, 0] = 0
    I[0, 0] = mininfinity
    # Filling first column and row I and M
    for i in xrange(n):
        I[i + 1, 0] = gapopen + i * gapext
        M[i + 1, 0] = mininfinity
    for j in xrange(m):
        I[0, j + 1] = gapopen + j * gapext
        M[0, j + 1] = mininfinity

    # Dynamic programming forward
    for i in xrange(n):
        for j in xrange(m):
            im = M[i, j + 1] + gapopen
            im = max(im, I[i, j + 1] + gapext)
            im = max(im, M[i + 1, j] + gapopen)
            im = max(im, I[i + 1, j] + gapext)
            I[i + 1, j + 1] = im
            if seqA[i] == seqB[j]:
                mm = max(M[i, j] + match, I[i, j] + match)
            else:
                mm = max(M[i, j] + mismatch, I[i, j] + mismatch)
            M[i + 1, j + 1] = mm
    score = max(M[n, m], I[n, m])

    # traceback
    alignA, alignB = [], []
    im, jm = n, m
    while im > 0 and jm > 0:
        Mdiag, MgapA, MgabB = M[im - 1, jm - 1], M[im, jm - 1], M[im - 1, jm]
        Idiag, IgapA, IgabB = I[im - 1, jm - 1], I[im, jm - 1], I[im - 1, jm]
        maxi = max(Mdiag, MgapA, MgabB, Idiag, IgapA, IgabB)
        if maxi == Mdiag or maxi == Idiag:
            im, jm = im - 1, jm - 1
            alignA.append(seqA[im])
            alignB.append(seqB[jm])
        elif maxi == MgapA or maxi == IgapA:
            jm = jm - 1
            alignA.append('-')
            alignB.append(seqB[jm])
        elif maxi == MgabB or maxi == IgabB:
            im = im - 1
            alignA.append(seqA[im])
            alignB.append('-')
    alignA.reverse()
    alignB.reverse()
    alignA = ''.join(alignA)
    alignB = ''.join(alignB)
    # we are computing global alignment so start is at 0 for both sequences and
    # we cover both sequences fully
    return (score, 0, 0, n - 1, m - 1, alignA, alignB)


def SmithWatermann(seqA, seqB, match, mismatch, gap):
    n, m = len(seqA), len(seqB)
    # forward Dynamic programming
    f = np.zeros((n + 1, m + 1))
    for i in xrange(n):
        for j in xrange(m):
            mx = 0
            if seqA[i] == seqB[j]:
                mx = max(f[i, j] + match, mx)
            else:
                mx = max(f[i, j] + mismatch, mx)
            mx = max(f[i, j + 1] + gap, mx)
            mx = max(f[i + 1, j] + gap, mx)
            f[i + 1, j + 1] = mx
    argm = np.argmax(f)
    im, jm = (argm // (m + 1), argm % (m + 1))
    score, endi, endj = f[im, jm], im, jm
    # traceback
    alignA, alignB = [], []
    while f[im, jm] > 0:
        diag, gapA, gabB = f[im - 1, jm - 1], f[im, jm - 1], f[im - 1, jm]
        maxi = max(diag, gapA, gabB)
        if  maxi == diag:
            im, jm = im - 1, jm - 1
            alignA.append(seqA[im])
            alignB.append(seqB[jm])
        elif maxi == gapA:
            jm = jm - 1
            alignA.append('-')
            alignB.append(seqB[jm])
        else:   # gabB
            im = im - 1
            alignA.append(seqA[im])
            alignB.append('-')
    alignA.reverse()
    alignB.reverse()
    alignA = ''.join(alignA)
    alignB = ''.join(alignB)

    return (score, im, jm, endi, endj, alignA, alignB)


def loadSeq(names):
    openfiles = [open(f, 'rb') for f in names]
    seq = [f.readline().strip() for f in openfiles]
    (f.close() for f in openfiles)
    return zip(names, seq)


def printCache(cache):
    for e in cache:
        s1, s2, info = e
        score, im, jm, endi, endj, alignA, alignB = info
        print 'Sequences %s and %s with score %d:\n' % (s1, s2, score)
        print '\tAligment is located at [%d:%d) for %s' % (im, endi, s1)
        print '\tAligment is located at [%d:%d) for %s' % (jm, endj, s2)
        print 'Alignment:'
        print '%s' % alignA
        print '%s' % alignB
        print '\n'


def test():
    match, mismatch, gap = 1, -1, -2
    names = ['t1.fasta', 't2.fasta']
    ns = loadSeq(names)  # (name,seq) list

    glob = NeedlemanWunsh(ns[0][1], ns[1][1], match, mismatch, gap)
    loc = SmithWatermann(ns[0][1], ns[1][1], match, mismatch, gap)
    cacheG = [(names[0], names[1], glob)]
    cacheL = [(names[0], names[1], loc)]
    print '\n====== Details in Needleman-Wunsch global alignment======\n'
    printCache(cacheG)
    print '\n====== Details in Smith-Waterman local alignment======\n'
    printCache(cacheL)


def task1ab():
    match, mismatch, gap = 1, -1, -2
    names = ['H.fasta', 'I.fasta', 'J.fasta']
    ns = loadSeq(names)  # (name,seq) list

    cacheG = []
    cacheL = []
    print '====== Needleman-Wunsch and Smith-Watermann ======'
    print ' glob/loc   ' + ' '.join(names)  # first row of table
    for (n1, s1) in ns:
        row = n1 + '    '
        for (n2, s2) in ns:
            if n1 == n2:
                row += ' skip '
                break  # do not compare same strings & the pairs again
            glob = NeedlemanWunsh(s1, s2, match, mismatch, gap)
            cacheG.append((n1, n2, glob))
            loc = SmithWatermann(s1, s2, match, mismatch, gap)
            cacheL.append((n1, n2, loc))
            row += '%d/%d ' % (glob[0], loc[0])
        print row

    print '\n====== Details in Needleman-Wunsch global alignment======\n'
    printCache(cacheG)
    print '\n====== Details in Smith-Waterman local alignment======\n'
    printCache(cacheL)


def task2():
    match, mismatch, gapopen, gapext = 1, -1, -2, -1
    names = ['h.fasta', 'm.fasta', 'r.fasta']
    ns = loadSeq(names)  # (name,seq) list

    cacheG = []
    print '====== Afinne Needleman-Wunsch ======'
    print ' glob   ' + ' '.join(names)  # first row of table
    for (n1, s1) in ns:
        row = n1 + '    '
        for (n2, s2) in ns:
            if n1 == n2:
                row += ' skip '
                break  # do not compare same strings & the pairs again
            glob = NWAffine(s1, s2, match, mismatch, gapopen, gapext)
            cacheG.append((n1, n2, glob))
            row += '%d ' % (glob[0],)
        print row

    print '\n====== Details in Afinne Needleman-Wunsch global alignment======\n'
    printCache(cacheG)


if __name__ == '__main__':
    task1ab()
    print '\n=== task2 =========\n'
    task2()
    # test()  # debugging
